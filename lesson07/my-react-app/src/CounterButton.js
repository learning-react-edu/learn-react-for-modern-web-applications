import { DangerButton } from "./Button";

export const CounterButton = ({ onIncrement, numberOfClicks = 0 }) => {
    return (
        <>
            <p>You've clicked the button {numberOfClicks} times</p>
            {/* < Button buttonColor='red' onClick={onIncrement}>Click Me!</Button> */}
            <DangerButton onClick={onIncrement}>Click Me!</DangerButton>
        </>
    );
}
