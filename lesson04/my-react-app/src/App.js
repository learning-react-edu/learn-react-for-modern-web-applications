import './App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import {
    HomePage,
    CounterButtonPage,
    PeopleListPage,
    NotFoundPage,
    ProtectedPage,
    ControlledFormPage,
    UncontrolledFormPage,
    UserProfilePage,
} from "./pages";
import { NavBar } from "./NavBar";
import { FormsNavBar } from "./FormsNavBar";

function App() {
  return (
    <div className="App">
        <Router>
            <NavBar />
            <div className="App-header">
                <Switch>
                    <Route path="/" exact>
                        <HomePage />
                    </Route>
                    <Route path="/counter/:name?">
                        <CounterButtonPage />
                    </Route>
                    <Route path="/people-list">
                        <PeopleListPage />
                    </Route>
                    <Route path="/protected">
                        <ProtectedPage />
                    </Route>
                    <Route path="/user">
                        <UserProfilePage />
                    </Route>
                    <Route path="/forms">
                        <Router>
                            <FormsNavBar />
                            <Route path="/controlled">
                                <ControlledFormPage />
                            </Route>
                            <Route path="/uncontrolled">
                                <UncontrolledFormPage />
                            </Route>
                        </Router>
                    </Route>
                    <Route>
                        <NotFoundPage />
                    </Route>
                </Switch>
            </div>
        </Router>
    </div>
  );
}

export default App;
