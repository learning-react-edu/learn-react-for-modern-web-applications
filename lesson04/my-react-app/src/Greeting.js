export const Greeting = ({ name, numberOfMessages }) => {
    if (!name) return null;
    const isMorning = (new Date()).getHours() < 12;
    const greetingHeader = isMorning ? 'Morning' : 'Evening';
    return (
        <>
            <h3>Good {greetingHeader}, {name}!</h3>
            {numberOfMessages === 0
                ? null
                : <p>You have {numberOfMessages} new message</p>}
        </>
    );
}
