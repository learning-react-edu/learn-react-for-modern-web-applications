import React, { Component } from 'react';

export class CounterButtonCB extends Component {
    render() {
        const { numberOfClicks, onIncrement } = this.props;
        return (
            <>
                <p>You've clicked the button {numberOfClicks} times</p>
                <button onClick={onIncrement}>Click Me!</button>
            </>
        );
    }
}
