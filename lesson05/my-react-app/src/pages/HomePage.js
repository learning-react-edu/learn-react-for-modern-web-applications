import { Greeting } from "../Greeting";

export const HomePage = () => (
    <>
        <h1>The Home Page</h1>
        <Greeting name="Edu" numberOfMessages={5}/>
    </>
);
