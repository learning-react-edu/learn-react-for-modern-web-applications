import { useHistory, useLocation } from 'react-router-dom';
import { parse } from "query-string";
import { useEffect } from "react";

export const ProtectedPage = () => {
    const { isAuthed } = parse(useLocation().search);
    const history = useHistory();

    useEffect(() => {
        if (isAuthed !== 'true') {
            history.push('/');
        }
    });

    return (
            <h1>Only authorised users should be able to see this!</h1>
        );
};
