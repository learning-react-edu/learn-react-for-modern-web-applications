import './App.css';
import { Greeting } from "./Greeting";
import {PeopleList} from "./PeopleList";

const people = [{
  name: 'John',
  age: 42,
  hairColor: 'brown',
}, {
  name: 'Helga',
  age: 25,
  hairColor: 'red',
}, {
  name: 'Dwayne',
  age: 55,
  hairColor: 'blonde',
}];

const displayAlert = () => {
  alert('Hello!');
}

function App() {
  const adjective = 'cool';
  const url = 'https://reactjs.org';

  return (
    <div className="App">
      <header className="App-header">
        <p style={{ color: 'violet', fontSize: '96px' }}>Big Text!</p>
        <Greeting name="Edu" numberOfMessages={5} />
        <PeopleList people={people} />
        <button onClick={displayAlert}>Click Me!</button>
        <button onClick={() => alert('Howdy!')}>Click Me Too!</button>
        <p>
          This is so {adjective}!
        </p>
        <a
          className="App-link"
          href={url}
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
