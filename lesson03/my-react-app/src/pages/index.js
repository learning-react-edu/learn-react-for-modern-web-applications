export { CounterButtonPage } from './CounterButtonPage';
export { HomePage } from './HomePage';
export { NotFoundPage } from './NotFoundPage';
export { PeopleListPage } from './PeopleListPage';
export { ProtectedPage } from './ProtectedPage';
