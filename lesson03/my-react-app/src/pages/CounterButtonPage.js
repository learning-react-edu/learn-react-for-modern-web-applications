import { useState } from "react";
import { useLocation, useParams } from "react-router-dom";
import { parse } from "query-string";
import { CongratulationsMessage } from "../CongratulationsMessage";
import { CounterButton } from "../CounterButton";

export const CounterButtonPage = () => {
    let { name } = useParams();
    name = !!name ? name + "'s" : '';
    const location = useLocation();
    const startingValue = Number(parse(location.search).startingValue) || 0;
    const [ numberOfClicks, setNumberOfClicks ] = useState(startingValue);
    const [ hideMessage, setHideMessage ] = useState(false);

    const increment = () => setNumberOfClicks(numberOfClicks + 1);

    return (
        <>
            <h1>{name} The Counter Button Page</h1>
            {hideMessage
                ? null
                : <CongratulationsMessage
                    numberOfClicks={numberOfClicks}
                    threshold={10}
                    onHide={() => setHideMessage(true)}
                />}
            <CounterButton onIncrement={increment} numberOfClicks={numberOfClicks}/>
        </>
    );
}
