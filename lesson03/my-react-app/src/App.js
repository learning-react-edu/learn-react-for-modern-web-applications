import './App.css';
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import { HomePage, CounterButtonPage, PeopleListPage, NotFoundPage, ProtectedPage } from "./pages";

function App() {
  return (
    <div className="App">
        <Router>
            <Link to="/">Go To Home Page</Link>
            <Link to="/counter">Go To Counter Page</Link>
            <Link to="/people-list">Go To People List Page</Link>
            <Switch>
                <Route path="/" exact>
                    <HomePage />
                </Route>
                <Route path="/counter/:name?">
                    <CounterButtonPage />
                </Route>
                <Route path="/people-list">
                    <PeopleListPage />
                </Route>
                <Route path="/protected">
                    <ProtectedPage />
                </Route>
                <Route>
                    <NotFoundPage />
                </Route>
            </Switch>
        </Router>
    </div>
  );
}

export default App;
